define([
    // Defaults
    "jquery"
], function ($) {



    // Avoid `console` errors in browsers that lack a console.
    (function() {
        var method;
        var noop = function () {};
        var methods = [
            'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
            'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
            'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
            'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
        ];
        var length = methods.length;
        var console = (window.console = window.console || {});

        while (length--) {
            method = methods[length];

            // Only stub undefined methods.
            if (!console[method]) {
                console[method] = noop;
            }
        }
    }());

// Place any jQuery/helper plugins in here.

    // ------------------------------------------------------------------------

    /*!
     * jQuery outside events - v1.1 - 3/16/2010
     * http://benalman.com/projects/jquery-outside-events-plugin/
     *
     * Copyright (c) 2010 "Cowboy" Ben Alman
     * Dual licensed under the MIT and GPL licenses.
     * http://benalman.com/about/license/
     */

    // Script: jQuery outside events
    //
    // *Version: 1.1, Last updated: 3/16/2010*
    //
    // Project Home - http://benalman.com/projects/jquery-outside-events-plugin/
    // GitHub       - http://github.com/cowboy/jquery-outside-events/
    // Source       - http://github.com/cowboy/jquery-outside-events/raw/master/jquery.ba-outside-events.js
    // (Minified)   - http://github.com/cowboy/jquery-outside-events/raw/master/jquery.ba-outside-events.min.js (0.9kb)
    //
    // About: License
    //
    // Copyright (c) 2010 "Cowboy" Ben Alman,
    // Dual licensed under the MIT and GPL licenses.
    // http://benalman.com/about/license/
    //
    // About: Examples
    //
    // These working examples, complete with fully commented code, illustrate a few
    // ways in which this plugin can be used.
    //
    // clickoutside - http://benalman.com/code/projects/jquery-outside-events/examples/clickoutside/
    // dblclickoutside - http://benalman.com/code/projects/jquery-outside-events/examples/dblclickoutside/
    // mouseoveroutside - http://benalman.com/code/projects/jquery-outside-events/examples/mouseoveroutside/
    // focusoutside - http://benalman.com/code/projects/jquery-outside-events/examples/focusoutside/
    //
    // About: Support and Testing
    //
    // Information about what version or versions of jQuery this plugin has been
    // tested with, what browsers it has been tested in, and where the unit tests
    // reside (so you can test it yourself).
    //
    // jQuery Versions - 1.4.2
    // Browsers Tested - Internet Explorer 6-8, Firefox 2-3.6, Safari 3-4, Chrome, Opera 9.6-10.1.
    // Unit Tests      - http://benalman.com/code/projects/jquery-outside-events/unit/
    //
    // About: Release History
    //
    // 1.1 - (3/16/2010) Made "clickoutside" plugin more general, resulting in a
    //       whole new plugin with more than a dozen default "outside" events and
    //       a method that can be used to add new ones.
    // 1.0 - (2/27/2010) Initial release
    //
    // Topic: Default "outside" events
    //
    // Note that each "outside" event is powered by an "originating" event. Only
    // when the originating event is triggered on an element outside the element
    // to which that outside event is bound will the bound event be triggered.
    //
    // Because each outside event is powered by a separate originating event,
    // stopping propagation of that originating event will prevent its related
    // outside event from triggering.
    //
    //  OUTSIDE EVENT     - ORIGINATING EVENT
    //  clickoutside      - click
    //  dblclickoutside   - dblclick
    //  focusoutside      - focusin
    //  bluroutside       - focusout
    //  mousemoveoutside  - mousemove
    //  mousedownoutside  - mousedown
    //  mouseupoutside    - mouseup
    //  mouseoveroutside  - mouseover
    //  mouseoutoutside   - mouseout
    //  keydownoutside    - keydown
    //  keypressoutside   - keypress
    //  keyupoutside      - keyup
    //  changeoutside     - change
    //  selectoutside     - select
    //  submitoutside     - submit

    (function($,doc,outside){
      '$:nomunge'; // Used by YUI compressor.

      $.map(
        // All these events will get an "outside" event counterpart by default.
        'click dblclick mousemove mousedown mouseup mouseover mouseout change select submit keydown keypress keyup'.split(' '),
        function( event_name ) { jq_addOutsideEvent( event_name ); }
      );

      // The focus and blur events are really focusin and focusout when it comes
      // to delegation, so they are a special case.
      jq_addOutsideEvent( 'focusin',  'focus' + outside );
      jq_addOutsideEvent( 'focusout', 'blur' + outside );

      // Method: jQuery.addOutsideEvent
      //
      // Register a new "outside" event to be with this method. Adding an outside
      // event that already exists will probably blow things up, so check the
      // <Default "outside" events> list before trying to add a new one.
      //
      // Usage:
      //
      // > jQuery.addOutsideEvent( event_name [, outside_event_name ] );
      //
      // Arguments:
      //
      //  event_name - (String) The name of the originating event that the new
      //    "outside" event will be powered by. This event can be a native or
      //    custom event, as long as it bubbles up the DOM tree.
      //  outside_event_name - (String) An optional name for the new "outside"
      //    event. If omitted, the outside event will be named whatever the
      //    value of `event_name` is plus the "outside" suffix.
      //
      // Returns:
      //
      //  Nothing.

      $.addOutsideEvent = jq_addOutsideEvent;

      function jq_addOutsideEvent( event_name, outside_event_name ) {

        // The "outside" event name.
        outside_event_name = outside_event_name || event_name + outside;

        // A jQuery object containing all elements to which the "outside" event is
        // bound.
        var elems = $(),

          // The "originating" event, namespaced for easy unbinding.
          event_namespaced = event_name + '.' + outside_event_name + '-special-event';

        // Event: outside events
        //
        // An "outside" event is triggered on an element when its corresponding
        // "originating" event is triggered on an element outside the element in
        // question. See the <Default "outside" events> list for more information.
        //
        // Usage:
        //
        // > jQuery('selector').bind( 'clickoutside', function(event) {
        // >   var clicked_elem = $(event.target);
        // >   ...
        // > });
        //
        // > jQuery('selector').bind( 'dblclickoutside', function(event) {
        // >   var double_clicked_elem = $(event.target);
        // >   ...
        // > });
        //
        // > jQuery('selector').bind( 'mouseoveroutside', function(event) {
        // >   var moused_over_elem = $(event.target);
        // >   ...
        // > });
        //
        // > jQuery('selector').bind( 'focusoutside', function(event) {
        // >   var focused_elem = $(event.target);
        // >   ...
        // > });
        //
        // You get the idea, right?

        $.event.special[ outside_event_name ] = {

          // Called only when the first "outside" event callback is bound per
          // element.
          setup: function(){

            // Add this element to the list of elements to which this "outside"
            // event is bound.
            elems = elems.add( this );

            // If this is the first element getting the event bound, bind a handler
            // to document to catch all corresponding "originating" events.
            if ( elems.length === 1 ) {
              $(doc).bind( event_namespaced, handle_event );
            }
          },

          // Called only when the last "outside" event callback is unbound per
          // element.
          teardown: function(){

            // Remove this element from the list of elements to which this
            // "outside" event is bound.
            elems = elems.not( this );

            // If this is the last element removed, remove the "originating" event
            // handler on document that powers this "outside" event.
            if ( elems.length === 0 ) {
              $(doc).unbind( event_namespaced );
            }
          },

          // Called every time a "outside" event callback is bound to an element.
          add: function( handleObj ) {
            var old_handler = handleObj.handler;

            // This function is executed every time the event is triggered. This is
            // used to override the default event.target reference with one that is
            // more useful.
            handleObj.handler = function( event, elem ) {

              // Set the event object's .target property to the element that the
              // user interacted with, not the element that the "outside" event was
              // was triggered on.
              event.target = elem;

              // Execute the actual bound handler.
              old_handler.apply( this, arguments );
            };
          }
        };

        // When the "originating" event is triggered..
        function handle_event( event ) {

          // Iterate over all elements to which this "outside" event is bound.
          $(elems).each(function(){
            var elem = $(this);

            // If this element isn't the element on which the event was triggered,
            // and this element doesn't contain said element, then said element is
            // considered to be outside, and the "outside" event will be triggered!
            if ( this !== event.target && !elem.has(event.target).length ) {

              // Use triggerHandler instead of trigger so that the "outside" event
              // doesn't bubble. Pass in the "originating" event's .target so that
              // the "outside" event.target can be overridden with something more
              // meaningful.
              elem.triggerHandler( outside_event_name, [ event.target ] );
            }
          });
        };

      };

    })(jQuery,document,"outside");

    // ------------------------------------------------------------------------

    /**
     * InView
     * author Remy Sharp
     * url http://remysharp.com/2009/01/26/element-in-view-event-plugin/
     * fork https://github.com/zuk/jquery.inview
     */
    (function($) {
        'use strict';

        function getScrollTop() {
            return window.pageYOffset ||
                document.documentElement.scrollTop ||
                document.body.scrollTop;
        }

        function getViewportHeight() {
            var height = window.innerHeight; // Safari, Opera
            // if this is correct then return it. iPad has compat Mode, so will
            // go into check clientHeight (which has the wrong value).
            if (height) {
                return height;
            }
            var mode = document.compatMode;

            if ((mode || !$.support.boxModel)) { // IE, Gecko
                height = (mode === 'CSS1Compat') ?
                    document.documentElement.clientHeight : // Standards
                    document.body.clientHeight; // Quirks
            }

            return height;
        }

        function offsetTop(debug) {
            // Manually calculate offset rather than using jQuery's offset
            // This works-around iOS < 4 on iPad giving incorrect value
            // cf http://bugs.jquery.com/ticket/6446#comment:9
            var curtop = 0;
            for (var obj = debug; obj; obj = obj.offsetParent) {
                curtop += obj.offsetTop;
            }
            return curtop;
        }

        function checkInView() {
            var viewportTop = getScrollTop(),
                viewportBottom = viewportTop + getViewportHeight(),
                elems = [];

            // naughty, but this is how it knows which elements to check for
            $.each($.cache, function() {
                if (this.events && this.events.inview) {
                    elems.push(this.handle.elem);
                }
            });

            $(elems).each(function() {
                var $el = $(this),
                    elTop = offsetTop(this),
                    elHeight = $el.height(),
                    elBottom = elTop + elHeight,
                    wasInView = $el.data('inview') || false,
                    offset = $el.data('offset') || 0,
                    inView = elTop > viewportTop && elBottom < viewportBottom,
                    isBottomVisible = elBottom + offset > viewportTop && elTop < viewportTop,
                    isTopVisible = elTop - offset < viewportBottom && elBottom > viewportBottom,
                    inViewWithOffset = inView || isBottomVisible || isTopVisible ||
                        (elTop < viewportTop && elBottom > viewportBottom);

                if (inViewWithOffset) {
                    var visPart = (isTopVisible) ? 'top' : (isBottomVisible) ? 'bottom' : 'both';
                    if (!wasInView || wasInView !== visPart) {
                        $el.data('inview', visPart);
                        $el.trigger('inview', [true, visPart]);
                    }
                } else if (!inView && wasInView) {
                    $el.data('inview', false);
                    $el.trigger('inview', [false]);
                }
            });
        }

        function createFunctionLimitedToOneExecutionPerDelay(fn, delay) {
            var shouldRun = false;
            var timer = null;

            function runOncePerDelay() {
                if (timer !== null) {
                    shouldRun = true;
                    return;
                }
                shouldRun = false;
                fn();
                timer = setTimeout(function() {
                    timer = null;
                    if (shouldRun) {
                        runOncePerDelay();
                    }
                }, delay);
            }

            return runOncePerDelay;
        }

        // ready.inview kicks the event to pick up any elements already in view.
        // note however, this only works if the plugin is included after the elements are bound to 'inview'
        var runner = createFunctionLimitedToOneExecutionPerDelay(checkInView, 100);
        $(window).on('checkInView.inview click.inview ready.inview scroll.inview resize.inview', runner);
    })(jQuery);

    // ------------------------------------------------------------------------

    /**
     * jQUery Limit Max Length
     */

    (function($){

    $.fn.limitMaxlength = function(options){

      var settings = jQuery.extend({
        attribute: "maxlength",
        onLimit: function(){},
        onEdit: function(){}
      }, options);

      // Event handler to limit the textarea
      var onEdit = function(){
        var textarea = $(this);
        var maxlength = parseInt(textarea.attr(settings.attribute));
        var lineFeeds = !$.isEmptyObject(textarea.val().match(/[^\n]*\n[^\n]*/gi)) ?
                  textarea.val().match(/[^\n]*\n[^\n]*/gi).length : 0;

        if(textarea.val().length > maxlength-lineFeeds){
          textarea.val(textarea.val().substr(0, maxlength-lineFeeds));

          // Call the onlimit handler within the scope of the textarea
          jQuery.proxy(settings.onLimit, this)();
        }

        // Call the onEdit handler within the scope of the textarea
        jQuery.proxy(settings.onEdit, this)(maxlength-lineFeeds - textarea.val().length);
      }

      this.each(onEdit);

      return this.keyup(onEdit)
            .keydown(onEdit)
            .focus(onEdit)
            .on('input paste', onEdit);
    }

    })(jQuery);


    // ------------------------------------------------------------------------

    /*
     * Copyright (c) 2009 George Mandis (georgemandis.com)
     */
    window.konami={input:"",clear:setTimeout('konami.clear_input()',2000),load:function(link){window.document.onkeyup=function(e){konami.input+=e?e.keyCode:event.keyCode
    if(konami.input=="3838404037393739666513"){konami.code(link)
    clearTimeout(konami.clear)}
    clearTimeout(konami.clear)
    konami.clear=setTimeout("konami.clear_input()",2000)}},code:function(link){window.location=link},clear_input:function(){konami.input="";clearTimeout(konami.clear);}}

    function createEasterEgg(){
        if($('#EasterEgg-Overlay').length<=0) {
            $('body').append('<div id="EasterEgg-Overlay"></div><div id="EasterEgg-Content"><div id="EasterEgg-Background"></div><div id="EasterEgg-Text"></div></div>');
        }

        var Overlay    = $('#EasterEgg-Overlay'),
            Content    = $('#EasterEgg-Content'),
            Background = $('#EasterEgg-Background'),
            KText      = $('#EasterEgg-Text');

        Overlay.css({
            'position':'fixed',
            'top':0,
            'left':0,
            'opacity':0.6,
            'height':'100%',
            'width':'100%',
            'z-index':65555,
            'background-color':'#000'
        }).bind('click',function(){
            Overlay.remove();
            Content.remove();
        });

        Content.css({
            'position':'fixed',
            'z-index':65556,
            'top':'50%',
            'left':'50%',
            //'height':'272px',
            'padding-bottom':'3px',
            'width':'241px',
            'margin-top':'-136px',
            'margin-left':'-120px',
            'background-color':'#fff'
        }).bind('click',function(){
            Overlay.remove();
            Content.remove();
        });

        Background.css({
            'height':'242px',
            'width':'241px',
            'background':'url(http://chilikode.com/upload/piyo.png) no-repeat 50% 50%'
        });

        KText.css({
            'width':'auto',
            'text-align':'center',
            'font-family':'Trebuchet MS',
            'marginTop':'10px',
            'color': '#000'
        }).html('Developed by:<br />Filiano, Giez, Danick, Leo');
    }

    $(function(){konami.code=function(){createEasterEgg();}
    konami.load();});
});

// Object Size
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

// Cohort pulse
// ------------------------------------------------------------------------

function jsCreateCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString()}else var expires="";document.cookie=name+"="+value+expires+"; path=/"}
function jsReadCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i!=ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(nameEQ)==0)return c.substring(nameEQ.length,c.length)}return null}
function jsGetCookie(cvar, cval) { if (cval == undefined) cval = ''; return (jsReadCookie(cvar) != null && jsReadCookie(cvar) != '') ? jsReadCookie(cvar) : cval; }
function jsRemoveCookie(name) { if (jsReadCookie(name) != null && jsReadCookie(name) != ''){ document.cookie=name+"=;expires=Thu, 01-Jan-1970 00:00:01 GMT;path=/"; } }
// function pulseCallback(o) { jsCreateCookie('checkpulse', o.pulse, 365); }