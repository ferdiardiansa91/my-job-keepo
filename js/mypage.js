var startDate;
var monthString = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des" ];

var pipeDate = function(date) {
    return date.getDate() + " " + monthString[date.getMonth()] + " " + date.getFullYear();
}

$(document).ready(function(){
    
    // ------------------------------------------------------------------------
    // Date Picker
    // ------------------------------------------------------------------------
    
    $(".datepicker").datepicker({
      dateFormat: "dd/mm/yy"
    });

    // ------------------------------------------------------------------------

    
    // ------------------------------------------------------------------------
    // Pull Down
    // ------------------------------------------------------------------------
    
    $(".pull-down a").click(function(){
        if ($(this).parent().hasClass('open')){
            $(this).parent().removeClass('open');
            $(this).parent().find(".option").stop().slideUp();
        } else{
            $(this).parent().addClass('open');
            $(this).parent().find(".option").stop().slideDown();
        }
    });

    $(".pull-down .option a").click(function(){
        $(this).parents(".pull-down").removeClass('open');
        $(this).parents(".pull-down").find('.option-selected').html($(this).text());
        $(this).parents(".option").slideUp();
    });

    $(".pull-down .submit-btn a").click(function(){
        startDate = $("#date-start").datepicker('getDate');
        endDate = $("#date-end").datepicker('getDate');
        console.log(pipeDate(startDate) + "-" + pipeDate(endDate));
        $(this).parents(".pull-down").find('.option-selected').html(pipeDate(startDate) + " - " + pipeDate(endDate));
    });


    $(".setting-content .button-area button[type=reset]").click(function(){
        $(this).parents(".setting-content").find('.input-gender .option-selected').html('select gender');
    });

    // ------------------------------------------------------------------------
    
    // ------------------------------------------------------------------------
    // Checkbox
    // ------------------------------------------------------------------------
    
    $(".check-box li a").click(function(){
        if ($(this).parent().hasClass('checked')){
            $(this).parent().removeClass('checked');
            $(this).find(".keepo-icon").removeClass("icon-check");
        } else{
            $(this).parent().addClass('checked');
            $(this).find(".keepo-icon").addClass("icon-check");
        }
    });

    // ------------------------------------------------------------------------

});