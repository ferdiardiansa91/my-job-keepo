define([
	// Defaults
	"jquery",
	"angular",
	"mediumEditor",
	"mediumInsert"
], function($, ng, MediumEditor){
	"use strict";

	var App = {
		mainEditor 		: void 0,
		listicleItems 	: [],

		_initEditor: function() {
			var self = this;

			// ------------------------------------------------------------------------
			// Module
			// ------------------------------------------------------------------------

			// Init Controller
			this.MainEditor.MainApp.keepoApp.controller('listicleController', ['$scope', '$element', function($scope, $element) {
				var windowListicle    = window.listicle || {};
				$scope.listicleOption = windowListicle.sort || 'ordinal';
				$scope.listicleItems  = windowListicle.models || [{"order": "1", "title": "", "image_str": "", "content": ""}];;

				// ------------------------------------------------------------------------

				// Set Listicle Order
				$scope.setOrder = function(event, order) {
					var $el = $(event.currentTarget || event.srcElement)

					if (!$el.length) { return false; }
					if ($scope.listicleOption == order) { return false; }

					$scope.listicleOption = order;

					// Rewrite numbering
					if (order != 'points') 
					{ this.rewriteNumber(-1); }

					// Change class
					$el.siblings('.active').removeClass('active');
					$el.addClass('active');
				};

				// ------------------------------------------------------------------------
				
				// Add Listicle Item
				$scope.addItem = function(event) {
					var $el 		= $(event.currentTarget || event.srcElement).closest('.eb-listicle-separator'),
						indexPos 	= 0;

					if (!$el.length) { return false; }

					// ------------------------------------------------------------------------
				
					// Get the order position
					if (! $el.prev('.eb-listicle-item').length) { indexPos = 0; }
					else { indexPos = $el.prev('.eb-listicle-item').index('.eb-listicle-item') + 1; }

					$scope.listicleItems.splice(indexPos, 0, {"order": (indexPos + 1), "title": "", "image_str": "", "content": ""});

					// ------------------------------------------------------------------------
					
					// Re-write numbering 
					this.rewriteNumber(indexPos);

					// Init Listicle Editor
					setTimeout(function() { $scope.initListicleEditor(indexPos); }, 50);
				};

				// ------------------------------------------------------------------------
				
				// Remove Listicle Item
				$scope.removeItem = function(event) {
					var $el 		= $(event.currentTarget || event.srcElement).closest('.eb-listicle-item'),
						indexPos 	= $el.index('.eb-listicle-item');

					if (!$el.length || (indexPos <= -1)) { return false; }

					// ------------------------------------------------------------------------
					
					$scope.listicleItems.splice(indexPos, 1);

					// empty? create new 
					if (! $scope.listicleItems.length) {
						$scope.listicleItems.splice(0, 0, {"order": 1, "title": "", "image_str": "", "content": ""});	

						// Init Listicle Editor
						setTimeout(function() { $scope.initListicleEditor(0); }, 50);
					}

					// ------------------------------------------------------------------------
					
					// Re-write numbering 
					this.rewriteNumber(indexPos - 1);
				};

				// ------------------------------------------------------------------------
				
				// Remove Listicle Item Image Preview
				$scope.removeItemPreview = function(event) {
					var $el 		= $(event.currentTarget || event.srcElement).closest('.eb-listicle-item'),
						indexPos 	= $el.index('.eb-listicle-item');

					if (!$el.length || (indexPos <= -1)) { return false; }

					// ------------------------------------------------------------------------

					$scope.listicleItems[indexPos].image_str = void 0;
				};

				// ------------------------------------------------------------------------
				
				// Initial Listicle Editor
				$scope.initListicleEditor = function(indexPos) {
					var $el = $element.find('.eb-listicle-list .eb-listicle-item:eq(' + indexPos + ')'),
						contentEditor;
					
					// Editor
					new self.MainEditor.titleEditorApp($el.find('.listicle-item-title'), {
						placeholder: "Title"
					});

					contentEditor = new MediumEditor($el.find('.listicle-item-content'), {
						toolbar: {
							buttons: ['bold', 'italic', 'underline', 'anchor', 'h1', 'h2', 'quote', "orderedlist", "unorderedlist"],
						},
						paste: {
							cleanPastedHTML: true,
							cleanTags: ["meta", "script", "style", "label"]
						},
						placeholder: {
							text: 'Write your content here ------- Write then block to show text tools'
						}
					});
					$el.find('.listicle-item-content').data('editor', contentEditor);

					$el.find('.listicle-item-content').mediumInsert({
				        editor: contentEditor,
				        addons: {
				        	images: {
				        		deleteScript: null,
				        		autoGrid: 0,
				        		fileUploadOptions: {
				        			url: self.MainEditor.uploadURL,
			        				beforeSend: function(xhr, data) {
			        			        var file = data.files[0],
			        			        	temp = JSON.parse(window.localStorage.token);

			        			        xhr.setRequestHeader('Authorization', temp.token_type + ' ' + temp.access_token);
			        			    }
				        		},
				        		styles: {
				        		    wide: { label: '<span class="icon-align-justify"></span>' },
				        		    left: null,//{ label: '<span class="icon-align-left"></span>' },
				        		    right: { label: '<span class="icon-align-right"></span>' },
				        		    grid: null
				        		}
				        	},
				        	embeds: {
				        		placeholder: 'Paste a YouTube, Facebook, Twitter, Instagram link/video and press Enter',
            					oembedProxy: '',
				        		styles: {
				        			wide: null,
				        			left: null,
				        			right: null
				        		}
				        	}
				        }
				    });

				    // FileUpload
				    self.MainEditor._initFileUpload($el.find('.fileupload-pool input[type=file]'), {dropZone: $el.find('.fileupload-pool')});
				};

				// ------------------------------------------------------------------------
				
				// Rewrite numbering
				$scope.rewriteNumber = function(indexPos) {
					switch ($scope.listicleOption) {
						case 'reverse':
							for (var i = ($scope.listicleItems.length - 1), j = 0; i >= 0; i--, j++) {
								$scope.listicleItems[j].order = i + 1;
							}
							break;
						default:
							for (var i = 0; i < $scope.listicleItems.length; i++) {
								$scope.listicleItems[i].order = i + 1;
							}
							break;
					}
				};
			}]);

			// ------------------------------------------------------------------------
			
			// Init editor
			setTimeout(function() {
				$.each($('.eb-listicle-list .eb-listicle-item'), function() {
					var $self = $(this),
						contentEditor;

					// Editor
					new self.MainEditor.titleEditorApp($self.find('.listicle-item-title'), {
						placeholder: "Title"
					});

					contentEditor = new MediumEditor($self.find('.listicle-item-content'), {
						toolbar: {
							buttons: ['bold', 'italic', 'underline', 'anchor', 'h1', 'h2', 'quote', "orderedlist", "unorderedlist"],
						},
						paste: {
							cleanPastedHTML: true,
							cleanTags: ["meta", "script", "style", "label"]
						},
						placeholder: {
							text: 'Write your content here ------- Write then block to show text tools'
						}
					});
					$self.find('.listicle-item-content').data('editor', contentEditor);

					$self.find('.listicle-item-content').mediumInsert({
				        editor: contentEditor,
				        addons: {
				        	images: {
				        		deleteScript: null,
				        		autoGrid: 0,
				        		fileUploadOptions: {
				        			url: self.MainEditor.uploadURL,
			        				beforeSend: function(xhr, data) {
			        			        var file = data.files[0],
			        			        	temp = JSON.parse(window.localStorage.token);

			        			        xhr.setRequestHeader('Authorization', 'Bearer ' + temp.token);
			        			    }
				        		},
				        		styles: {
				        		    wide: { label: '<span class="icon-align-justify"></span>' },
				        		    left: null,//{ label: '<span class="icon-align-left"></span>' },
				        		    right: { label: '<span class="icon-align-right"></span>' },
				        		    grid: null
				        		}
				        	},
				        	embeds: {
				        		placeholder: 'Paste a YouTube, Facebook, Twitter, Instagram link/video and press Enter',
            					oembedProxy: '',
				        		styles: {
				        			wide: null,
				        			left: null,
				        			right: null
				        		}
				        	}
				        }
				    });

					// FileUpload
				    self.MainEditor._initFileUpload($self.find('.fileupload-pool input[type=file]'), {dropZone: $self.find('.fileupload-pool')});
				});
			}, 50);

			// ------------------------------------------------------------------------
			
			// Re-adjust MainEditor
			this.MainEditor.prepSave = this.prepSave;

			// ------------------------------------------------------------------------

			// start up the module
			this.MainEditor.MainApp.keepoApp.run();
			angular.bootstrap(document.querySelector("html"), ["keepoApp"]);
		},

		init: function(MainEditor) {
			this.MainEditor = MainEditor;

			// Init Main Editor
			this.MainEditor._initEditor();

			// Init Listicle Editor
			this._initEditor();
		},

		prepSave: function(data, draft) {
			var self 		= this,
				draft 		= draft || false,
				prepData 	= {},
				$listicleScope, listicleItems;
			if (this.$mainContainer.hasClass('on-progress')) { return false; }

			// ------------------------------------------------------------------------

			// Set up listicle content
			$listicleScope = angular.element('[ng-controller=listicleController]').scope();
			listicleItems  = [];
			$.each($listicleScope.listicleItems, function(index) {
				var $element 		= self.$mainBody.find('.eb-listicle-list'),
					$listicleEl 	= $($element).find('.eb-listicle-item:eq(' + index + ')'),
					contentEditor 	= $listicleEl.find('.listicle-item-content').data('editor');

				listicleItems.push({
					order 		: this.order,
					title 		: $listicleEl.find('.listicle-item-title').text(),
					image_str 	: $listicleEl.find('.listicle-item-image input[name=fn]').val() || '',
					content 	: contentEditor.serialize()['element-0'].value.replace(/contenteditable(=(\"|\')true(\"|\')|)/ig, '')
				});
			});
			
			// Set up the data
			prepData = {
				cover 		: data.image.id,
				title 		: data.title ? data.title : 'Untitled',
				lead 		: data.lead,
				content 	: JSON.stringify({
									content 	: data.content,
									sort 		: $listicleScope.listicleOption,
									models 		: listicleItems
							  }),
				tags 		: data.tags.join(';'),
				source 		: data.source ? ((/^http\:\/\//).test(data.source) ? data.source : 'http://' + data.source) : '',
				channel 	: data.channel.slug,
				type 		: this.editorApp,
				status  	: draft ? 'draft' : 'submit',
				slug 		: data.slug
			};

			// ------------------------------------------------------------------------
			
			this.$mainContainer.addClass('on-progress');
			this.save(prepData);
		}
	};

	//window.editorData = {"title": "Test listice ", "lead": "Lead", "content":"<h2>Ngasih kejutan pake acara ngelempar telur, tepung, air, ato apapun ke temen yang lagi ngerayain hari jadinya emang udah jadi kebiasaan dan wajar di kalangan remaja Indonesia. Biasanya sih yang ultah oke-oke aja dan ngerasa seneng coz artinya temen-temen mereka perhatian. Tapi kalo kejutan ultahnya segini parahnya, ya lain dong ya ceritanya.</h2><h2><b>Udah 2 hari in fesbuk dibikin geger sama foto-foto kejutan ultah sekumpulan ABG yang masih duduk di bangku SMA</b>. Apa pasal? Yup, foto ini jadi viral gegara kejutan yang mereka bikin adalah kejutan yang kelewatan.</h2><p><br></p><p><br></p>", "category": {"slug": "lol-channel", "name": "Anime, Comic & Manga"}, "source": "http://google.com", "slug": "test2"};
	//window.listicle = {"sort":"ordinal","models":[{"order":"1","title":"Sekelompok murid SMA ini kelewatan banget deh ngerjain temennya yang ultah","image_str":"http://media.keepo.me/keepo.me-13892204_1155372211189597_1933971978324341534_n1.jpg","content":"<h4>Di foto itu terlihat ada 7 murid yang masih pake baju pramuka lengkap. <b>Menurut keterangan beberapa sumber sih mereka adalah murid jurusan TKJ di SMK PAB 10 Patumbak</b>. Ceritanya sih mereka lagi ngasih kejutan buat ngerayain ultah salah satu temen mereka itu. Tapi ya ampuuun, kejutannya kelewatan banget deh ah. </h4><p><br></p>"},{"order":"2","title":"Kejutan ini lebih mirip bullying","image_str":"http://media.keepo.me/keepo.me-13934843_1155372297856255_458081432324642711_n1.jpg","content":"<h4>Kalo diliat-liat dari foto yang beredar sih, menurut ane kejutan ultah ini kok <b>rada nyerempet ke arah bullying</b> gitu ya? Lah gimana enggak, liat tuh yang ultah tangan dan badannya diiket ke tiang, jilbabnya dilepas, disiram pake air dan ditimpukin telor. Iyuhhh</h4><h4>Kok ya pada kejem banget gitu sih? Kalian bukan titisan HItler kan?<br></h4><br><p><br></p>"},{"order":"3","title":"Kalo yg dikasih kejutan ampe begini, masih bisa disebut \"cuma seru-seruan kok\"?","image_str":"http://media.keepo.me/keepo.me-14045876_1155372361189582_5281864654151815403_n1.jpg","content":"<h4>Ets, bahkan di salah satu foto itu terlihat kalo ada temen mereka yang ngejambak rambut si korban. Apalagi kalo kita ngeliat ekspresi wajah si korban sampe nangis-nangis gitu, kayaknya mustahil banget deh kalo ini cuma becandaan doang.</h4><h4>Ya ampun, itu anak orang neng, bukan kecebong!</h4><p><br></p>"},{"order":"4","title":"Kelakuan preman murid-murid itu pun bikin netizen gerah","image_str":"http://media.keepo.me/keepo.me-14040202_1155372424522909_357834789138846476_n1.jpg","content":"<h4>Nah, foto-foto sadis ini makin viral dan mancing emosi netizen se-Indonesia Raya merdeka-merdeka setelah salah satu fans page mengunggah ulang foto-foto tersebut. Situ udah tau lah yeee gimana reaktifnya netizen kita. Dikit-dikit meledak, bentar-bentar ngamuk. Hahaaa</h4><p><br></p>"},{"order":"5","title":"Akhirnya... Dihujat netizen lah mereka","image_str":"http://media.keepo.me/keepo.me-13912693_1155372227856262_5483014758665137190_n1.jpg","content":"<h4>Kelakuan mereka itu pun bikin <b>netizen sejagad Nusantara mencak-mencak</b>. Maka sumpah serapah beserta kata-kata kasar mengandung hewan berkaki empat keluar dari mulut mereka. Nggak tanggung-tanggung, kandangnya sekalian dibawa! Ane baca komen-komennya aja mendadak merinding! Sumpah, saking seremnya kecaman mereka.</h4><p><br></p>"},{"order":"6","title":"Jadiin pelajaran aja, yak...","image_str":"http://media.keepo.me/keepo.me-13902772_1155372177856267_4112461090482935758_n3.jpg","content":"<h4>Kejadian yang kayak beginian nih yang kudu kita jadiin pelajaran hidup baik-baik. <b>Hal apa pun, meskipun bagi kita niatnya cuma iseng dan main-main, niat kita nggak mesti buat orang lain diartikan sama. </b>Makanya jadi orang nggak usah macem-macem dan kebanyakan tingkah kayak gitu. Dibully netizen baru nyaho, loe.</h4><h4><b>Kayak ane dong, Gan. Kalem dan memesona! *uhuk*</b></h4><p><br></p><p><b>Yoo, pada mlipir ke issues yang keren ini</b></p><ul><li><a href=\"http://keepo.me/news-info-channel/risma-nyindir-jakarta-ahok-nggak-terima--gitu-aja-terus-sampe-metallica-ngeluarin-album-religi\" target=\"_blank\">Risma Nyindir Jakarta, Ahok Nggak Terima. Gitu Aja Terus Sampe Metallica Ngeluarin Album Religi</a></li><li><a href=\"http://keepo.me/news-info-channel/duh-desain-logo-hut-ri-71-mirip-banget-sama-logo-infanteri-amerika--kira-kira-njiplak-nggak-ya\" target=\"_blank\">Duh, Desain Logo HUT RI 71 Mirip Banget sama Logo Infanteri Amerika. Kira-Kira Njiplak Nggak Ya?</a></li><li><a href=\"http://keepo.me/news-info-channel/SCTV-dan-Indosiar-Lebih-Prioritas-Nayangin-Sinetron-dan-FTV-Ketimbang-Atlit-Kita-yang-Berlaga-di-Olimpiade-Siapa-yang-Gak-Ngamuk\" target=\"_blank\">SCTV dan Indosiar Lebih Prioritas Nayangin Sinetron dan FTV Ketimbang Atlit Kita yang Berlaga di Olimpiade, Siapa yang Gak Ngamuk?</a></li><li><a href=\"http://keepo.me/news-info-channel/gedeg-sama-ulah-tuyul-yang-sering-ngutil-duit-warga-di-kota-ini-sampe-pasang-baliho-peringatan-duh-segitunya\" target=\"_blank\">Gedeg sama Ulah Tuyul yang Sering Ngutil Duit, Warga di Kota Ini sampe Pasang Baliho Peringatan! Duh, Segitunya...</a></li><li><a href=\"http://keepo.me/news-info-channel/Murid-Nggak-Ngerjain-PR-Eh-Gurunya-yang-Kena-Bogem-Mentah-Jangan-Jangan-Kita-Emang-Beneran-Butuh-Full-Day-School-Nih\" target=\"_blank\">Murid Nggak Ngerjain PR, Eh Gurunya yang Kena Bogem Mentah. Jangan-Jangan Kita Emang Beneran Butuh Full-Day School Nih?</a></li></ul><p><br></p><p><br></p>"}]}

	return App;
});