$(document).ready(function() {
	$('#top-caption').on('keyup', function() {
		var value 	= $(this).val();
		$('.text-top-caption').text(value);
	});

	$('#bottom-caption').on('keyup', function() {
		var value 	= $(this).val();
		$('.text-bottom-caption').text(value);
	});

	$('.text-top-caption, .text-bottom-caption').on('dblclick', function() {
		$(this).toggleClass('is-active');
	});


	$("#color").spectrum({
		color: "#fff",
	    showInput: true,
	    move: function(c) {
	        $(".is-active").css('color', c.toHexString());
	    }
	});


	$('#outline').spectrum({
		showInput : true,
		move : function(c) {
			$('.is-active').css('-webkit-text-stroke-color', c.toHexString());
		}
	});

	$('#size').on('change', function() {
		var size 	= $('#size').val();
		$('.is-active').css('font-size', size);
	});

	// COOSE IMAGE

	$('.container-img > img').on('click', function() {
		var src 	= $(this).attr('src');
		var cutSrc	= src.replace('img/','');

		chooseImage(src);
	});
});


$(function() {
	$('.text-top-caption, .text-bottom-caption').draggable({
		axis 			: 'x y',
		containment		: "#preview-gambar"
	});
});

$(document).on('change','input[type="file"]', function() {
	// var value	= $(this).val();
	// progressBarSim(0);
	previewGambar(this);
})

function progressBarSim(al,file) {
	var bar     = document.getElementById('progressBar');
	var status  = document.getElementById('status');
	status.innerHTML = al+'%';
	bar.value   = al;
	al++;

	var sim   = setTimeout('progressBarSim('+al+')', 50);
	if( al > 100) {
	  status.innerHTML  = '100%';
	  clearTimeout(sim);
	  previewGambar(file);
	}

	return true;
}

function previewGambar(input) {
	if(input.files && input.files[0]) {
	  var reader  = new FileReader();
	  var size 	  = input.files[0].size;
  	  var img     = new Image;

	img.onload = function() {
	  	alert(img.width);
	}

	  if(size > 1024 * 1024) {
     	  $('.alert.alert-danger').fadeIn('fast');
	  }else {
		  $('.alert.alert-danger').fadeOut('fast');
		  reader.onload = function(e) {

		    $('.preview').attr('src', e.target.result);
		    $('.remove-image').fadeIn('slow');
		  }
		  reader.readAsDataURL(input.files[0]);
		  $('.eb-meme-upload-holder').fadeOut(1000);
	  }

	}
}

function hapusGambar() {
	$('.preview').attr('src','');
	$('.remove-image').fadeOut('fast');
    $('.eb-meme-upload-holder').fadeIn('fast');
    $('#top-caption').val('');
    $('#bottom-caption').val('');
}

function chooseImage(input) {
	// CLOSE MODAL
	$('.bs-example-modal-sm').modal('hide');
	
	// APPAND IMAGE
	$('.preview').attr('src', input);
	$('.remove-image').fadeIn('slow');

	// HIDE BUTTON
	$('.alert.alert-danger').fadeOut('fast');
	$('.eb-meme-upload-holder').fadeOut(1000);
}

