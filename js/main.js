requirejs.config({
    urlArgs: 'v=0.0.0.1',
    waitSeconds: 60000,
    paths: {
        jquery              : 'vendor/jquery-1.12.0.min',
        angular             : 'vendor/angular.min',
        angularRoute        : 'vendor/angular-route.min',
        angularSanitize     : 'vendor/angular-sanitize.min',
        
        underscore          : 'vendor/underscore',
        sanitize            : 'vendor/sanitize',

        dante               : 'vendor/dante-editor',
        handlebar           : 'vendor/medium/handlebars.runtime',
        mediumEditor        : 'vendor/medium/medium-editor',
        mediumInsert        : 'vendor/medium/medium-editor-insert-plugin',
        jquerySortable      : 'vendor/medium/jquery-sortable-min',
        blueimpUpload       : 'vendor/bluimp-upload/jquery.fileupload-ui',
        ngTags              : 'vendor/ng-tags-input',

        jqueryUI            : 'vendor/jquery-ui/jquery-ui.min',
        jScrollPane         : 'vendor/jquery.jscrollpane.min',
        mouseWheel          : 'vendor/jquery.mousewheel',
        rangyInputs         : 'vendor/rangyinputs-jquery',

        bxSlider            : 'vendor/bxslider/jquery.bxslider.min',
        bxSliderEasing      : 'vendor/bxslider/jquery.easing.1.3',
        bxSliderFitVid      : 'vendor/bxslider/jquery.fitvids',

        // Social Media
        facebook            : '//connect.facebook.com/en_US/sdk',
    },
    shim: {
        angularSanitize     : { deps: ["angular"] },
        dante               : { deps: ["jquery", "underscore", "sanitize"] },
        blueimpUpload       : { deps: ["underscore"] },
        mediumInsert        : { deps: ["jquery", "mediumEditor", "handlebar", "jquerySortable", "blueimpUpload"] },
        ngTags              : { deps: ["angular"] },
        jqueryUI            : { deps: ["jquery"] },
        jScrollPane         : { deps: ["jquery", "mouseWheel"] },
        rangyInputs         : { deps: ["jquery"] },
        bxSlider            : { deps: ["jquery", "bxSliderEasing", "bxSliderFitVid"] },
        sticker             : { deps: ["jScrollPane", "rangyInputs"] },
        facebook            : { export: "FB" }
    }
});

{parsetags: 'explicit'};
require(['jquery', 'app', 'plugins', 'common', 'angular'], function($, MainApp) {
    MainApp.init();
});
